-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 20, 2022 at 02:26 AM
-- Server version: 10.4.20-MariaDB
-- PHP Version: 8.0.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `taplist`
--
CREATE DATABASE IF NOT EXISTS `taplist` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `taplist`;

-- --------------------------------------------------------

--
-- Table structure for table `beer`
--

CREATE TABLE `beer` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `abv` varchar(10) NOT NULL,
  `og` float DEFAULT NULL,
  `fg` float DEFAULT NULL,
  `style` varchar(255) NOT NULL,
  `rgb` varchar(10) NOT NULL,
  `description` text NOT NULL,
  `added_dt` datetime NOT NULL,
  `removed_dt` datetime NOT NULL,
  `active_ind` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `beer_tap_reltn`
--

CREATE TABLE `beer_tap_reltn` (
  `id` int(11) NOT NULL,
  `tap_num` int(11) NOT NULL,
  `beer_id` int(11) NOT NULL,
  `scale_num` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `config`
--

CREATE TABLE `config` (
  `id` int(11) NOT NULL,
  `item` varchar(255) NOT NULL,
  `val` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `pour_history`
--

CREATE TABLE `pour_history` (
  `id` int(11) NOT NULL,
  `tap_num` int(11) NOT NULL,
  `beer_id` int(11) NOT NULL,
  `pour_oz` float NOT NULL,
  `pour_dt_tm` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tap_fill`
--

CREATE TABLE `tap_fill` (
  `id` int(11) NOT NULL,
  `tap_num` int(11) NOT NULL,
  `fill_oz` int(11) NOT NULL,
  `fill_pct` int(11) NOT NULL,
  `last_pour` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `beer`
--
ALTER TABLE `beer`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `beer_tap_reltn`
--
ALTER TABLE `beer_tap_reltn`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `config`
--
ALTER TABLE `config`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pour_history`
--
ALTER TABLE `pour_history`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tap_fill`
--
ALTER TABLE `tap_fill`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `beer`
--
ALTER TABLE `beer`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `beer_tap_reltn`
--
ALTER TABLE `beer_tap_reltn`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `config`
--
ALTER TABLE `config`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `pour_history`
--
ALTER TABLE `pour_history`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tap_fill`
--
ALTER TABLE `tap_fill`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
