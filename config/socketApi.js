var socket_io = require('socket.io');
var io = socket_io();
var socketApi = {};
var conn = require('./db').connection();
var cron = require('node-cron');
var request = require('request');
var async = require('async');

var conn = require('../config/db').connection();

socketApi.io = io;

var taps = [];
var pours = [];

conn.query('SELECT * FROM tap_fill ORDER BY tap_num ASC', function(err, results) {
	for(var i = 0; i < results.length; i++) {
		taps.push(results[i].fill_oz);
		pours.push(results[i].last_pour);
	}

})

var scales = [
	// Put API keys in below:

	//1
	'',
	//2
	'',
	//3
	'',
	//4
	''
]

io.once('connection', function(socket) {  
	cron.schedule('*/10 * * * * *', function() {

		conn.query('SELECT btr.*, tf.fill_oz FROM beer_tap_reltn btr JOIN tap_fill tf on tf.tap_num = btr.tap_num WHERE btr.scale_num IS NOT NULL', function(err, taplist) {
			if(err) console.log(err);

			async.each(taplist, function(tap) {


				var key = scales[tap.scale_num - 1];

				request.get('http://plaato.blynk.cc/' + key + '/get/v51', function(err, tap_response) {
					if(err) console.log(err);

					var fill_oz = Math.floor(JSON.parse(tap_response.body) * 128)

					var pct = Math.floor((fill_oz / 640) * 100);

					update_fill(tap.tap_num, fill_oz, tap.fill_oz, key, tap.beer_id, function(response) {
						if(response.did_change == 'oz') {
							io.sockets.emit('update_no_pour', tap.tap_num, pct, fill_oz, response.pour);
						} 

						if(response.did_change == 'pour') {
							io.sockets.emit('update', tap.tap_num, pct, fill_oz, response.pour);
						}
					})


				})
			}, function(err) {
				if(err) console.log(err);
			})

		})

		//co2
		request.get('http://plaato.blynk.cc/<co2 scale api code>/get/v51', function(err, co2) {
			if(err) console.log(err);

			var pct = Math.floor((JSON.parse(co2.body) / 20) * 100)

			io.sockets.emit('update_co2', pct);

		})
	});

	socket.on('disconnect', function() {
		console.log('disconnect')
		socket.disconnect();
	})
});

function update_fill(tap, oz_cur, oz_db, key, beer_id, cb) {
	request.get('http://plaato.blynk.cc/' + key + '/get/v59', function(err, response) {

		var pour = Math.floor(JSON.parse(response.body) * 10) / 10
		var pour_detail = Math.floor(JSON.parse(response.body) * 1000) / 1000

		if(pour_detail != pours[tap-1] && pour >= 1.9) {
			conn.query('UPDATE tap_fill SET last_pour = ?,  fill_oz = ? WHERE tap_num = ?', [pour_detail, oz_cur, tap], function(err, p_results) {	
				conn.query('INSERT INTO pour_history(tap_num, beer_id, pour_oz) VALUES(?,?,?)', [tap, beer_id, pour_detail], function(err) {
					console.log('(Pour Detail) old pour: ' + pours[tap-1] + ' - new pour: ' + pour_detail);
					taps[tap-1] = oz_cur;
					pours[tap-1] = pour_detail;
					return cb({did_change: 'pour', pour: pour});				
				});
			});

		} else if(oz_cur + 12 <= oz_db || oz_cur - 12 >= oz_db) {
			conn.query('UPDATE tap_fill SET last_pour = ?,  fill_oz = ? WHERE tap_num = ?', [pour_detail, oz_cur, tap], function(err, p_results) {
				conn.query('INSERT INTO pour_history(tap_num, beer_id, pour_oz) VALUES(?,?,?)', [tap, beer_id, pour_detail], function(err) {
					console.log('(Cur oz.) old oz: ' + oz_db + ' - new oz: ' + oz_cur);
					taps[tap-1] = oz_cur;
					pours[tap-1] = pour_detail;
					return cb({did_change: 'oz', pour: pour})						
				});				
			});
	
		} else {
			return cb({did_change: false})
		}
	});
}

module.exports = socketApi;