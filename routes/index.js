var express = require('express');
var router = express.Router();

var conn = require('../config/db').connection();

var cron = require('node-cron');

var moment = require('moment')

/* GET home page. */
router.get('/', function(req, res, next) {

	conn.query('SELECT b.*, r.tap_num, r.scale_num, tf.fill_oz FROM beer b JOIN beer_tap_reltn r on r.beer_id = b.id JOIN tap_fill tf ON tf.tap_num = r.tap_num WHERE b.active_ind = 1 ORDER BY r.tap_num ASC', function(err, results) {
		conn.query('SELECT * FROM config', function(err, config) {
			if(err) console.log('err: ', err);

			var time = config.find(o => o.item === 'last_updated')
			var time_str = moment(time.val).fromNow();

			var taps = [];

			for(var i = 1; i < 6; i++) {
				var pushed = 0;
				for(var j = 0; j < results.length; j++) {
					if(i == results[j].tap_num) {
						results[j].fill_pct = Math.floor((results[j].fill_oz / 640) * 100);
						results[j].bottles = Math.floor(results[j].fill_oz / 12);
						taps.push(results[j]);
						pushed = 1;
					}
				}

				if(pushed == 0) taps.push(0);
			}

			res.render('index', { title: '<Brewery Name>', fonts: fonts, taps: taps, last_updated: time_str});			
		})

	})
  
});

router.get('/admin', function(req, res, next) {

	conn.query('SELECT b.*, r.tap_num FROM beer b JOIN beer_tap_reltn r on r.beer_id = b.id WHERE b.active_ind = 1 ORDER BY r.tap_num ASC', function(err, results) {
		if(err) console.log('err: ', err);

		var taps = [];
		var pushed = 0;

		for(var i = 1; i < 6; i++) {
			pushed = 0;
			for(var j = 0; j < results.length; j++) {
				if(i == results[j].tap_num) {
					taps.push(results[j]);
					pushed = 1;
				}
			}

			if(pushed == 0) taps.push(0);
		}

		console.log('taps: ', taps);

		var oz = [];
		var pct = [];
		var pours = [];

		conn.query('SELECT * FROM tap_fill ORDER BY tap_num ASC', function(err, results) {
			for(var i = 0; i < results.length; i++) {

				oz.push(results[i].fill_oz);
				pct.push(Math.floor((results[i].fill_oz / 640) * 100));
				pours.push(results[i].last_pour);
			}

			console.log(pct);

			res.render('admin', {title: 'Admin - The Black Rabbit', taps: taps, pct: pct, pours: pours, oz: oz});
		})

		
	})

});

router.post('/new_beer', function(req, res, next) {

	if(req.body.og == '') req.body.og = null;
	if(req.body.fg == '') req.body.fg = null;
	if(req.body.scale_num == '') req.body.scale_num = null;

	var beer_vals = [req.body.beer_name, req.body.abv, req.body.og, req.body.fg, req.body.style, req.body.rgb, req.body.description, 1];

	console.log('body: ', req.body)

	conn.query('INSERT INTO beer(name, abv, og, fg, style, rgb, description, active_ind) VALUES(?,?,?,?,?,?,?,?)', beer_vals, function(err, new_beer) {
		if(err) console.log(err);
		var new_beer_id = new_beer.insertId;

		conn.query('INSERT INTO beer_tap_reltn(tap_num, beer_id, scale_num) VALUES(?,?,?); UPDATE config SET val = CURDATE() WHERE item = "last_updated";', [req.body.tap_num, new_beer_id, req.body.scale_num], function(err, results) {
			res.redirect('/admin');
		});
    });
})

router.post('/kick_beer', function(req, res, next) {
	var tap = req.body.kick_num;
	console.log('tap: ' +  tap);

	conn.query('SELECT id, beer_id FROM beer_tap_reltn WHERE tap_num = ?', tap, function(err, results) {
		if(err) console.log(err)
		conn.query('DELETE FROM beer_tap_reltn WHERE id = ?; UPDATE beer SET active_ind = 0 WHERE id = ?; UPDATE config SET val = CURDATE() WHERE item = "last_updated";' +
					'UPDATE tap_fill SET fill_oz = 0, fill_pct = 0, last_pour = 0 WHERE tap_num = ?', [results[0].id, results[0].beer_id, tap], function(err, dresults) {
			if(err) console.log(err);
			res.redirect('/admin');
		})
	})
});

router.get('/get_keg_info', function(req, res, next) {
	var tap = req.query.tap;

	console.log('tap: ' + tap);

	conn.query('SELECT beer_id FROM beer_tap_reltn WHERE tap_num = ?', tap, function(err, results) {
		console.log('results: ', results);
		conn.query('SELECT * FROM beer WHERE id = ?', results[0].beer_id, function(err, beer_info) {
			res.json(beer_info[0]);
		})
	})
});

router.post('/update_beer', function(req, res, next) {
	conn.query('UPDATE beer SET name = ?, abv = ?, og = ?, fg = ?, style = ?, rgb = ?, description = ? WHERE id = ?', 
		[req.body.beer_name, req.body.abv, req.body.og, req.body.fg, req.body.style, req.body.rgb, req.body.description, req.body.beer_id], function(err, results) {
			if(err) console.log(err);
			res.redirect('/admin');
		})
})

module.exports = router;
